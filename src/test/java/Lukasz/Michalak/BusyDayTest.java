package Lukasz.Michalak;


import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertTrue;

public class BusyDayTest {

    private BusyDay busyDay;
    private Notification notification1;
    private Notification notification2;
    private Notification notification3;

    @Before
    public void setup() {
        busyDay = new BusyDay(7, 10, 2017);
        notification1 = new Notification("Shopping", "08:20", "09:00");
        notification2 = new Notification("Washing Dishes", "09:15", "09:45");
        notification3 = new Notification("Java Studying", "10:00", "15:30");

    }

    @Test
    public void testAddNotification() {
        busyDay.addNotification(notification3);
        boolean result = busyDay.getToDoList().contains(notification3);
        assertTrue(result);


    }

    @Test
    public void testRemoveNotification() {
        busyDay.getToDoList().add(notification2);
        busyDay.removeNotification(notification2);
        boolean result = busyDay.getToDoList().isEmpty();
        assertTrue(result);
    }

    @Test
    public void testRemoveNotificationByDescription() {
        busyDay.getToDoList().add(notification1);
        busyDay.removeNotificationByDescription("Shopping");
        boolean result = busyDay.getToDoList().isEmpty();
        assertTrue(result);

    }

    @Test
    public void testPostponeNotoficationToday() {
        busyDay.getToDoList().add(notification1);
        busyDay.postponeNotificationToday(notification1, "09:00", "09:00");
        String result1 = notification1.getStartHour();
        String result2 = notification1.getEndHour();
        assertThat(result1).isEqualTo(result2).isEqualTo("09:00");

    }

    @Test
    public void testRemoveAllNotifications() {
        busyDay.getToDoList().add(notification1);
        busyDay.getToDoList().add(notification2);
        busyDay.removeAllNotification();
        boolean result = busyDay.getToDoList().isEmpty();
        assertTrue(result);
    }

    @Test
    public void testReturnSorted() {
        busyDay.getToDoList().addAll(Arrays.asList(notification3, notification2, notification1));
        String result = busyDay.returnSorted().get(0).getStartHour();
        assertThat(result).isEqualTo("08:20");
    }
}

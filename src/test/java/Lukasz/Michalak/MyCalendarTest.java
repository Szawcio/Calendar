package Lukasz.Michalak;


import org.junit.Before;
import org.junit.Test;

import static junit.framework.TestCase.assertFalse;
import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.Assert.assertTrue;

public class MyCalendarTest {

    private MyCalendar calendar;
    private BusyDay busyDay;

    @Before
    public void setup() {
        calendar = new MyCalendar();
        busyDay = new BusyDay(29,7,2017);
    }

    @Test
    public void testDayOfWeek() {
        int result = calendar.dayOfWeek(1, 7, 2017);
        assertThat(result).isEqualTo(6);
    }

    @Test
    public void testIsLeapYear() {
        boolean result = calendar.isLeapYear(2016);
        assertThat(result).isTrue();
    }

    @Test
    public void testaddBusyDay(){
        calendar.addBusyDay(busyDay);
        boolean result = calendar.getBusyDays().contains(busyDay);
        assertTrue(result);
    }

    @Test
    public void testremoveByDay(){
        calendar.addBusyDay(busyDay);
        calendar.removeBusyDay(busyDay);
        boolean result = calendar.getBusyDays().contains(busyDay);
        assertFalse(result);
    }

    @Test
    public void testRemoveByDate(){
        calendar.addBusyDay(busyDay);
        calendar.removeByDate(29,7,2017);
        int result = calendar.getBusyDays().size();
        assertThat(result).isEqualTo(0);

    }


}

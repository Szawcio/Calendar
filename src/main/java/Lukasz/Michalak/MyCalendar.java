
package Lukasz.Michalak;

import org.joda.time.LocalDate;
import org.joda.time.LocalDateTime;
import org.joda.time.LocalTime;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Scanner;

public class MyCalendar {


    private String[] months = {"", "January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
    private int[] days = {0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31};

    private List<BusyDay> busyDays;


    public MyCalendar() {
        busyDays = new ArrayList<>();

    }

    public void addBusyDay(BusyDay busyDay) {
        busyDays.add(busyDay);
    }

    public void removeBusyDay(BusyDay busyDay) {
        busyDays.remove(busyDay);
    }

    public void removeByDate(int day, int month, int year) {
        boolean found = false;
        LocalDate temp = new LocalDate(year, month, day);
        Iterator<BusyDay> iterator = busyDays.iterator();
        while (iterator.hasNext()) {
            BusyDay busyDay = iterator.next();
            if (busyDay.getLocalDate().isEqual(temp)) {
                System.out.println("Your busy day: " + busyDay.getLocalDate() + " is free now :)");
                iterator.remove();
                found = true;
            }
        }
        if (!found) {
            System.out.println("No busy day found!");
        }
    }


    public void printCalendarForMonth(int month, int year) {
        if (month == 2 && isLeapYear(year)) {
            days[month] = 29;
        }
        System.out.println("        " + months[month] + " " + year);
        System.out.println("Sun Mon Tue Wed Thu Fri Sat");
        int d1 = dayOfWeek(1, month, year);
        for (int i = 0; i < d1; i++)
            System.out.print("    ");
        for (int i = 1; i <= days[month]; i++) {
            System.out.printf("%2d  ", i);
            if (((i + d1) % 7 == 0) || (i == days[month])) System.out.println();

        }

    }


    public int dayOfWeek(int day, int month, int year) {
        int y = year - (14 - month) / 12;
        int x = y + y / 4 - y / 100 + y / 400;
        int m = month + 12 * ((14 - month) / 12) - 2;
        int d = (day + x + (31 * m) / 12) % 7;
        return d;
    }

    public boolean isLeapYear(int year) {
        if ((year % 4 == 0) && (year % 100 != 0)) {
            return true;
        }
        if (year % 400 == 0) {
            return true;
        }
        return false;
    }

    public void printCalForYear(int year) {
        for (int i = 1; i <= 12; i++) {
            printCalendarForMonth(i, year);
            System.out.println();

        }
    }

    public void printInfo() {
        System.out.println("Hello! I am your personal calendar :)");
        System.out.println();
        LocalTime timer = LocalTime.now();
        String time = String.valueOf(timer).substring(0, 5);
        System.out.println("Today is: " + today() + "   " + "==(" + time + ")==");
        System.out.println("Numbers of all your busy days: " + busyDays.size());
        int busyDaysLeft = 0;
        int busyDaysBehind = 0;
        for (BusyDay busyDay : busyDays) {
            if (busyDay.getLocalDate().isAfter(today())) {
                busyDaysLeft++;
            }
            if (busyDay.getLocalDate().isBefore(today())) {
                busyDaysBehind++;
            }
        }
        System.out.println("You had: " + busyDaysBehind + " busy days, so far");
        System.out.println("Still got: " + busyDaysLeft + " busy days");
        isTodayABusyDay();
    }

    public LocalDate today() {
        LocalDate today = LocalDate.now();
        return today;
    }

    public void isTodayABusyDay() {
        boolean found = false;
        for (BusyDay busyDay : busyDays) {
            if (busyDay.getLocalDate().equals(today())) {
                System.out.println("Today is busy day!");
                busyDay.printToDoListFromEarliestNotif();
            }
        }
        if (!found) {
            System.out.println("You don't have any notifications for today");
        }
    }

    public void printToDoListForDay(LocalDate localDate){
        for (BusyDay busyDay : busyDays){
            if (busyDay.getLocalDate().equals(localDate)){
                busyDay.printToDoListFromEarliestNotif();
            }
        }
    }

    public void saveToTxtFile() throws FileNotFoundException {

        File file = new File("myBusyDays.txt");
        PrintWriter save =new PrintWriter(file);
        save.println("<busyDays>");
        for (BusyDay busyDay : busyDays) {
            save.println(busyDay.getLocalDate());
            for (Notification notification : busyDay.getToDoList()) {
                save.println(notification.getDescription()+","+notification.getStartHour()+","+notification.getEndHour());
            }
            save.println("/"+busyDay.getLocalDate());
        }
        save.println("</busyDays>");
        save.close();
    }

    public void loadFromTxtFile() throws FileNotFoundException {
        File file = new File("myBusyDays.txt");
        Scanner sc = new Scanner(file);
        while (sc.hasNextLine()){
            String temp = sc.nextLine();
            if (temp.equals("<busyDays>")){
                System.out.println("Loading busy days....");
            }
            if (temp.contains("-")&&temp.contains("20")){
                LocalDate tempDate = new LocalDate(temp);
                int tempDay = tempDate.getDayOfMonth();
                int tempMonth = tempDate.getMonthOfYear();
                int tempYear = tempDate.getYear();
                BusyDay tempBusyDay = new BusyDay(tempDay,tempMonth,tempYear);
                while (sc.hasNextLine()){
                    String temp2 = sc.nextLine();
                    if (temp2.charAt(0)=='/'){
                        break;
                    }
                    String[] parts =temp2.split(",");
                    String desc = parts[0];
                    String startH = parts[1];
                    String endH = parts[2];
                    Notification tempNoti = new Notification(desc,startH,endH);
                    tempBusyDay.addNotification(tempNoti);
                }
                busyDays.add(tempBusyDay);

            }
        }

    }


    public String[] getMonths() {
        return months;
    }

    public void setMonths(String[] months) {
        this.months = months;
    }

    public int[] getDays() {
        return days;
    }

    public void setDays(int[] days) {
        this.days = days;
    }

    public List<BusyDay> getBusyDays() {
        return busyDays;
    }

    public void setBusyDays(List<BusyDay> busyDays) {
        this.busyDays = busyDays;
    }
}
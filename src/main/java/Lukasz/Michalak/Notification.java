package Lukasz.Michalak;


public class Notification {

    private String description;
    private String startHour;
    private String endHour;

    public Notification(String description, String startHour, String endHour) {
        this.description = description;
        this.startHour = startHour;
        this.endHour = endHour;
    }



    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getStartHour() {
        return startHour;
    }

    public void setStartHour(String startHour) {
        this.startHour = startHour;
    }

    public String getEndHour() {
        return endHour;
    }

    public void setEndHour(String endHour) {
        this.endHour = endHour;
    }
}

package Lukasz.Michalak;


import org.joda.time.LocalDate;


import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

import static java.util.Comparator.comparing;


public class BusyDay {

    private LocalDate localDate;
    private List<Notification> toDoList;


    public BusyDay(int day, int month, int year) {
        localDate = new LocalDate(year, month, day);
        toDoList = new ArrayList<>();
    }


    public void addNotification(Notification notification) {
        toDoList.add(notification);
        System.out.println(notification.getDescription() + " alert, added correctly");

    }

    public void removeNotification(Notification notification) {
        System.out.println(notification.getDescription() + " alert, removed correctly");
        toDoList.remove(notification);
    }

    public void removeNotificationByDescription(String desctription) {
        boolean found = false;
        Iterator<Notification> iterator = toDoList.iterator();
        while (iterator.hasNext()) {
            Notification notification = iterator.next();
            if (desctription.equals(notification.getDescription())) {
                System.out.println(notification.getDescription() + " alert, removed correctly");
                iterator.remove();
                found = true;
            }
        }
        if (!found) {
            System.out.println("No notofication found!");
        }

    }

    public void postponeNotificationToday(Notification notification, String newStartHour, String newEndHour) {
        notification.setStartHour(newStartHour);
        notification.setEndHour(newEndHour);
    }

    public void removeAllNotification() {
        toDoList.clear();
    }


    public List<Notification> returnSorted() {
        List<Notification> temporary = new ArrayList<>();
        temporary.addAll(toDoList);
        temporary.sort(comparing(Notification::getStartHour));
        return temporary;

    }

    public void printToDoListFromEarliestNotif() {
        System.out.println(getLocalDate() + " do:");
        for (Notification notification : returnSorted()) {
            System.out.println(notification.getStartHour() + " - " + notification.getEndHour() + " " + notification.getDescription());
        }
    }

    public void printToDoListFromLatestNotif() {
        System.out.println(getLocalDate() + " do:");
        List<Notification> reversed = new ArrayList<>();
        reversed.addAll(returnSorted());
        Collections.reverse(reversed);
        for (Notification notification : reversed) {
            System.out.println(notification.getStartHour() + " - " + notification.getEndHour() + " " + notification.getDescription());
        }
    }








    public LocalDate getLocalDate() {
        return localDate;
    }

    public void setLocalDate(LocalDate localDate) {
        this.localDate = localDate;
    }

    public List<Notification> getToDoList() {
        return toDoList;
    }

    public void setToDoList(List<Notification> toDoList) {
        this.toDoList = toDoList;
    }
}
